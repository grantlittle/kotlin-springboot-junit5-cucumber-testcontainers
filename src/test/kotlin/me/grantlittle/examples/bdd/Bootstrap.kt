@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")

package me.grantlittle.examples.bdd

import io.cucumber.java.AfterAll
import io.cucumber.java.BeforeAll
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.cucumber.junit.platform.engine.Constants
import io.cucumber.spring.CucumberContextConfiguration
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.assertAll
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.IncludeEngines
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.web.reactive.function.client.WebClient
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.time.Duration


@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("me/grantlittle/examples/bdd") // This selector is picked up by Cucumber
@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "pretty")
@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "usage")
@ConfigurationParameter(key = Constants.PLUGIN_PROPERTY_NAME, value = "html:target/cucumber-reports.html")
class IntegrationTests

@Suppress("unused")
class StepDefinitions {

	private var createCustomer: CreateCustomer? = null


	@Given("some customer details")
	fun some_customer_details() {
		// Write code here that turns the phrase above into concrete actions
		createCustomer = CreateCustomer(
			firstName = "John",
			lastName = "Doe",
			email = "john.doe@example.com",
		)

	}

	@LocalServerPort
	private var serverPort: Int? = null

	private var customerId: String? = null

	@When("I invoke the create customer operation with the customer details")
	fun i_invoke_the_create_customer_operation_with_the_customer_details() {
		val webClient = WebClient.create("http://localhost:$serverPort")
		val customer = webClient
			.post()
			.uri("/api/1.0/customers")
			.bodyValue(createCustomer!!)
			.retrieve()
			.bodyToMono(Customer::class.java)
			.block(Duration.ofSeconds(5))

		assertNotNull(customer)
		assertAll(
			{ assertNotNull(customer!!.id) },
			{ assertEquals("John", customer!!.firstName) },
			{ assertEquals("Doe", customer!!.lastName) },
			{ assertEquals("john.doe@example.com", customer!!.email) }
		)
		customerId = customer!!.id


	}

@Autowired
lateinit var customerRepository: CustomerRepository

@Then("the customer details should be persisted")
fun the_customer_details_should_be_persisted() {

	val customer = customerRepository.findById(customerId!!).orElse(null)

	assertNotNull(customer)
	assertAll(
		{ assertNotNull(customer.id) },
		{ assertEquals("John", customer.firstName) },
		{ assertEquals("Doe", customer.lastName) },
		{ assertEquals("john.doe@example.com", customer.email) }
	)
}
}



@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@CucumberContextConfiguration
@Testcontainers
object Bootstrap {

	@Container
	@ServiceConnection
	var postgresContainer: PostgreSQLContainer<*> = PostgreSQLContainer(DockerImageName.parse("postgres"))
}

@BeforeAll
fun setUp() {
	Bootstrap.postgresContainer
		.withReuse(true)
		.start()

}

@AfterAll
fun tearDown() {
	Bootstrap.postgresContainer.stop()
}
package me.grantlittle.examples.bdd

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
@EntityScan
class KotlinSpringJUnit5CucumberApplication {

}

fun main(args: Array<String>) {
	runApplication<KotlinSpringJUnit5CucumberApplication>(*args)
}

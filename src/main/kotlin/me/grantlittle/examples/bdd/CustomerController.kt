package me.grantlittle.examples.bdd

import jakarta.persistence.*
import org.springframework.data.repository.CrudRepository
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers

@RestController
@RequestMapping("/api/1.0/customers")
class CustomerController(
    private val customerRepository: CustomerRepository
) {

    @PostMapping(consumes = ["application/json"], produces = ["application/json"])
    fun createCustomer(@RequestBody createCustomer: CreateCustomer): Mono<Customer> {
        return createCustomer
            .let {
                CustomerEntity(
                    firstName = it.firstName,
                    lastName = it.lastName,
                    email = it.email
                )
            }
            .let {
                //Don't inline blocking call
                Mono.fromCallable { customerRepository.save(it) }.subscribeOn(Schedulers.boundedElastic())
            }
            .map {
                Customer(
                    id = it.id!!,
                    firstName = it.firstName!!,
                    lastName = it.lastName!!,
                    email = it.email!!
                )
            }

    }
}

@Entity
@Table(name = "customer")
open class CustomerEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    open var id: String? = null,
    open var firstName: String? = null,
    open var lastName: String? = null,
    open var email: String? = null
)

interface CustomerRepository: CrudRepository<CustomerEntity, String>


package me.grantlittle.examples.bdd

data class CreateCustomer(
    val firstName: String,
    val lastName: String,
    val email: String
)

data class Customer(
    val id: String,
    val firstName: String,
    val lastName: String,
    val email: String
)

